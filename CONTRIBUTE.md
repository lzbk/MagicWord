This app is a quite standard symfony3 one. Depending if you're in dev or prod environnement and depending on the nature of your changes, you need to launch some commands if you want to test them. *./application/Makefile* file already contains useful "shortcuts". Feel free to add yours.

Here are some tips :

* If you remove/add/edit entities, you will certainly have to update your database.
> make dbupdate

* If you remove/add/edit javascript files, you'll need to launch grunt (and maybe edit *./application/Gruntfile.js*).
> grunt

* In prod environnement, most of changes will require clearing the cache
> make cache


* If you've made a lot of changes you can update/clearing/compiling almost everything with :
> make update
