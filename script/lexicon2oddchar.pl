#script listant les char non-alphabétiques de $fileLexicon
use Encode;
use IO::Handle;
STDOUT->autoflush();
use strict;
use utf8;
use Data::Dumper;


##################################DEBUT STRUCTURE DE DONNEES ########################################

	#######CHEMIN RESSOURCES
my $dirRessource="entries-ru";
my $fileLexicon="lexicon.tsv";

my %oddchar;
##################################FIN STRUCTURE DE DONNEES ########################################

open(LOG,">:encoding(utf8)","log-tsv.txt");




################################## DEBUT LECTURE LEXIQUE ENTRIES   #######################################

open(LEXICON,"<:encoding(utf8)",$dirRessource."/".$fileLexicon);
print LOG "LECTURE $fileLexicon \n";
while (my $line = <LEXICON>) {
	if($line=~/^([^\t]+)\t/){
		my @chars=split("",$1);
	foreach my $char (@chars){
		if( ($char=~/\P{L}/) && (!exists($oddchar{$char})) ){
			$oddchar{$char}=1;
			print LOG "ONE  -> ".$char."\n";
		}
	}
	}
	
}
close(LEXICON);


################################## FIN LECTURE LEXIQUE ENTRIES  #######################################

close(LOG);