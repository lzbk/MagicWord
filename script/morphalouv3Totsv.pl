#script prenant en entrée les ressources suivantes : 
	#Dans rep /lexiques ($dirRessource), les fichiers avec nom .+\..+ (pattern $nameLexiconFiles=
use Encode;
use IO::Handle;
STDOUT->autoflush();
use strict;
use utf8;
use Data::Dumper;


################################## VARIABLE LEXIQUE USER ########################################
my $userLanguage="frenchM3";

################################## VARIABLE LEXIQUE USER  ########################################


##################################DEBUT STRUCTURE DE DONNEES ########################################
my $dirRessource="lexiques";
my $nameLexiconFiles="[^\.]+";
	#######CHEMIN RESSOURCES
my $fileLexiconIn="Morphalou-2.0.xml";
# my $fileLexiconIn="extrait.xml";
my $fileLexiconOut="lexicon.tsv";


my %roots2features;#$roots2features{"id"}=feat1=value1,feat2=value2;
my %roots2lemma;#$roots2lemma{"id"}=$elmma;
my %wordsLexicon;#$wordsLexicon{"id"}{} = "abajar	cat=V,type=M,mood=I,tense=I,person=3,num=P,gen=0"

##################################FIN STRUCTURE DE DONNEES ########################################

open(LOG,">:encoding(utf8)","loglst.txt");

#Début lecture 

my $inRules=0;
my $line;


################################## DEBUT LECTURE LEXIQUE ENTRIES   #######################################

open(LEXICONTSV,">:encoding(utf8)",$fileLexiconOut);


opendir(LEXICONS,$dirRessource);
while(my $fileLexicon = readdir(LEXICONS)){
	if($fileLexicon=~/$nameLexiconFiles$/){
		open(LEXICON,"<:encoding(utf8)",$dirRessource."/".$fileLexicon);

		my $currentID="";
		my $currentForm="";
		my $inLE=0;
		my $inLF=0;
		my $inIF=0;
		while ($line = <LEXICON>) {
			if($line=~/^\s*<lexicalEntry id="(.+)">/){
				$currentID=$1;
				$inLE=1;
				$inIF=0;
				print LOG "Passage LA $currentID \n";
			}
			elsif($line=~/^\s*<\/lexicalEntry>/){
				$inLE=0;
				my @inflectedForms=keys(%{$wordsLexicon{$currentID}});
				foreach my $inflectedForm (@inflectedForms) {
					$wordsLexicon{$currentID}{$inflectedForm}.=",".$roots2features{$currentID};
					print LOG "Passage LU $currentForm \n";
					print LEXICONTSV $inflectedForm."\t";
					print LEXICONTSV $roots2lemma{$currentID}."\t";
					print LEXICONTSV $wordsLexicon{$currentID}{$inflectedForm}."\n";
				}
				undef %wordsLexicon;
				undef %roots2lemma;
				undef %roots2features;
			}
			elsif($line=~/^\s*<lemmatizedForm>/){
				$inLF=1;
			}
			elsif($line=~/^\s*<\/lemmatizedForm>/){
				$inIF=0;
			}
			elsif($line=~/^\s*<inflectedForm>/){
				$inIF=1;
			}
			elsif($line=~/^\s*<orthography[^<>]*>(.+)</){
				$currentForm=$1;
				print LOG "Passage LO $currentForm \n";
				#$wordsLexicon{currentID}{$currentForm}="";
				if(!$inIF){
					$roots2lemma{$currentID}=$currentForm;
				}
				
			}
			elsif($line=~/^\s*<(grammatical[^<>]+)>(.+)</){
				my $feature=$1;
				my $value=$2;
				
				if($inIF){
					if(exists($wordsLexicon{$currentID}{$currentForm})){
						$wordsLexicon{$currentID}{$currentForm}.=",".$feature."=".$value;
					}
					else{
						$wordsLexicon{$currentID}{$currentForm}=$feature."=".$value;
					}
				}
				elsif($inLF){
					if(exists($roots2features{$currentID})){
						$roots2features{$currentID}.=",".$feature."=".$value;
					}
					else{
						$roots2features{$currentID}=$feature."=".$value;
					}
				}
			}
		}
		close(LEXICON);
	}
}
closedir(LEXICONS);
################################## FIN LECTURE LEXIQUE ENTRIES  #######################################


################################## DEBUT ECRITURE LEXIQUE TSV   #######################################



close(LEXICONTSV);
################################## FIN ECRITURE LEXIQUE TSV #######################################



close(LOG);