#script prenant en entrée les ressources suivantes : 
	#$fileLexiconIn
use Encode;
use IO::Handle;
STDOUT->autoflush();
use strict;
use utf8;
use Data::Dumper;
use locale;



################################## VARIABLE LEXIQUE USER  ########################################


##################################DEBUT STRUCTURE DE DONNEES ########################################
my $dirRessource="lexiques";
my $nameLexiconFiles="[^\.]+";
	#######CHEMIN RESSOURCES
my $fileLexiconIn="lexicon-demi.tsv";
# my $fileLexiconIn="extrait.xml";
my $fileLexiconOut="lexicon-quart.tsv";




open(LEXICON,"<:encoding(utf8)",$fileLexiconIn);
my @lines=<LEXICON>;

close(LEXICON);



open(LEXICONTSV,">:encoding(utf8)",$fileLexiconOut);

for (my $var = int($#lines/4); $var < $#lines; $var++) {
	print LEXICONTSV $lines[$var];
}

close(LEXICONTSV);
################################## FIN ECRITURE LEXIQUE TSV #######################################