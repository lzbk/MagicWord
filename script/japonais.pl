#script prenant en entrée les ressources suivantes : 
	#le fichier xml fileLexiconIn

	#en sortie -> lexicon.tsv ($fileLexiconOut)
use Encode;
use IO::Handle;
STDOUT->autoflush();
use strict;
use utf8;
use Data::Dumper;


################################## VARIABLE LEXIQUE USER ########################################
my $userLanguage="frenchM2";

################################## VARIABLE LEXIQUE USER  ########################################


##################################DEBUT STRUCTURE DE DONNEES ########################################

	#######CHEMIN RESSOURCES
my $fileLexiconIn="japonais.tsv";
# my $fileLexiconIn="extrait.xml";
my $fileLexiconOut="lexicon-japan.tsv";


##################################FIN STRUCTURE DE DONNEES ########################################

#Début lecture 

my $line;


################################## DEBUT LECTURE LEXIQUE ENTRIES   #######################################

open(LEXICONTSV,">:encoding(utf8)",$fileLexiconOut);

open(LEXICON,"<:encoding(utf8)",$fileLexiconIn);

# while ( ($line = <LEXICON>) && ($cptID <= 1000) ) {
while ( ($line = <LEXICON>) ) {
	if($line=~/^([^\t]+)\t([^\t]+)\t([^\t]+)$/){
		my ($form,$combo,$feats) = ($1,$2,$3); 
		# if($feats=~/^(\S+) (.+)$/){
		# 	$feats = "pos=".$2;
		# }
		print LEXICONTSV $form."\t".$combo."\tpos=".$feats;
	}
	else{
		print "Ligne chelou $line";
	}
}
close(LEXICON);

################################## FIN LECTURE LEXIQUE ENTRIES  #######################################

close(LEXICONTSV);
