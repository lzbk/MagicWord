#script prenant en entrée les ressources suivantes : 
	#$fileLexiconIn
use Encode;
use IO::Handle;
STDOUT->autoflush();
use strict;
use utf8;
use Data::Dumper;
use locale;



################################## VARIABLE LEXIQUE USER  ########################################


##################################DEBUT STRUCTURE DE DONNEES ########################################
my $dirRessource="lexiques";
my $nameLexiconFiles="[^\.]+";
	#######CHEMIN RESSOURCES
my $fileLexiconIn="lexicon.tsv";
# my $fileLexiconIn="extrait.xml";
my $fileLexiconOut="lexicon-sort.tsv";




open(LEXICON,"<:encoding(utf8)",$fileLexiconIn);
my @lines=<LEXICON>;

close(LEXICON);



open(LEXICONTSV,">:encoding(utf8)",$fileLexiconOut);

print LEXICONTSV join("",sort(@lines));

close(LEXICONTSV);
################################## FIN ECRITURE LEXIQUE TSV #######################################

