#script prenant en entrée les ressources suivantes : 
	#Dans rep /entries, les fichiers avec nom .+\..+ (pattern $nameLexiconFiles="MM.(adj|adv|int|nom|vaux|verb)")
#		Ex : en espagnol MM.(adj|adv|int|nom|vaux|verb)
		#+ le tagset.data qui explique les catégories et traits

	#en sortie -> lexicon.tsv (le lexique sous la forme word\troot\feat1=value,feat2=value...)
use Encode;
use IO::Handle;
STDOUT->autoflush();
use strict;
use utf8;
use Data::Dumper;


################################## VARIABLE LEXIQUE USER ########################################
my $userLanguage="frenchM";
my $userDescription="ce lexique recouvre les mots  ... et les traits";
my $userRelationType="dérivation, racine, etc ... définition par l'user";


################################## VARIABLE LEXIQUE USER  ########################################


##################################DEBUT STRUCTURE DE DONNEES ########################################

	#######CHEMIN RESSOURCES
my $fileLexiconIn="lexiques/commonNoun_Morphalou3.1_LMF.xml";
my $fileLexiconOut="lexicon.tsv";
my $fileSpecOut="spec.txt";
#my $nameLexiconFiles="lefff\.(adj|adv|int|nom|vaux|verb)";
my $nameLexiconFiles="[^\.]+";


my %wordsLexicon;#$wordsLexicon{"abajaban"} = "abajar	cat=V,type=M,mood=I,tense=I,person=3,num=P,gen=0"

##################################FIN STRUCTURE DE DONNEES ########################################

open(LOG,">:encoding(utf8)","loglst.txt");

#Début lecture 

my $line;

my %tags;

################################## DEBUT LECTURE LEXIQUE ENTRIES   #######################################

open(LEXICON,"<:encoding(utf8)",$fileLexiconIn);

my $currentID="";
my $inLE=0;
my $inLF=0;
while ($line = <LEXICON>) {
	if($line=~/^\s*<([^\s<>]+)[^><]*>/){
		$tags{$1}=1;
	}
}
close(LEXICON);

################################## FIN LECTURE LEXIQUE ENTRIES  #######################################


################################## DEBUT ECRITURE LEXIQUE TSV   #######################################

open(LEXICONTSV,">:encoding(utf8)","tagsMorphalouN.txt");

my @tagsstring=sort { $a cmp $b } keys(%tags);
foreach my $tagstring (@tagsstring){
	print LEXICONTSV $tagstring."\n";
}


close(LEXICONTSV);
################################## FIN ECRITURE LEXIQUE TSV #######################################



close(LOG);