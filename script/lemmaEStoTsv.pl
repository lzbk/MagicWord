#script prenant en entrée les ressources suivantes :
	#le fichier xml fileLexiconIn

	#en sortie -> lexicon.tsv ($fileLexiconOut)
use Encode;
use IO::Handle;
STDOUT->autoflush();
use strict;
use utf8;
use Data::Dumper;


################################## VARIABLE LEXIQUE USER ########################################
my $userLanguage="frenchM2";

################################## VARIABLE LEXIQUE USER  ########################################


##################################DEBUT STRUCTURE DE DONNEES ########################################

	#######CHEMIN RESSOURCES
my $fileLexiconIn="lemmatization-es.txt";
# my $fileLexiconIn="extrait.xml";
my $fileLexiconOut="lexicon-es.tsv";


##################################FIN STRUCTURE DE DONNEES ########################################

#Début lecture

my $line;


################################## DEBUT LECTURE LEXIQUE ENTRIES   #######################################

open(LEXICONTSV,">:encoding(utf8)",$fileLexiconOut);

open(LEXICON,"<:encoding(utf8)",$fileLexiconIn);

# while ( ($line = <LEXICON>) && ($cptID <= 1000) ) {
while ( ($line = <LEXICON>) ) {
	if($line=~/^([^\t]+)\t([^\t]+)$/){
		my ($root, $form) = ($1,$2);
		# if($feats=~/^(\S+) (.+)$/){
		# 	$feats = "pos=".$2;
		# }
		$form =~s/\s+$//g;
		print LEXICONTSV $form."\t".$root."\tfeat=null\n";
	}
	else{
		print "Ligne chelou $line";
	}
}
close(LEXICON);

################################## FIN LECTURE LEXIQUE ENTRIES  #######################################

close(LEXICONTSV);
