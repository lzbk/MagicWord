use Encode;
use IO::Handle;
STDOUT->autoflush();
use strict;
use utf8;
use Data::Dumper;
use Text::Levenshtein qw(distance);
use Data::Dumper;
use experimental 'smartmatch';
use locale;


my $fileNBA = "Seasons_Stats.csv";
my $fileLexicon = "lexicon.tsv";


open( NBA, "<:encoding(utf8)", $fileNBA );
my @linesNBA=<NBA>;
close(NBA);
open( LEXICON, ">:encoding(utf8)", $fileLexicon );
my $cpt=0;
foreach my $lineNBA (@linesNBA){
	if($cpt != 0){
		if($lineNBA=~/^[^,]*,([^,]*),([^,]+),([^,]*),([^,]*),([^,]*),/){
			my ($year, $names, $post, $age, $team) = ($1,$2,$3,$4,$5);
			if($names =~ /^(.+) (.+)$/ ){
				my ($firstame, $surname) = ($1,$2);
				print LEXICON $firstame."\t".$team."-".$year."\tage=".$age."post=".$post."\n";
				print LEXICON $surname."\t".$team."-".$year."\tage=".$age."post=".$post."\n";
			}
			else{
				print "Name ? $names dans $lineNBA \n";
			}
		}
	}
	$cpt++;

}
close LEXICON;
