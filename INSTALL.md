Installation & configuration
============================

Requirements
------------
* docker-ce :
   * https://docs.docker.com/install/linux/docker-ce/ubuntu/
   * https://docs.docker.com/install/linux/docker-ce/debian/
   * When installing docker-ce, if you are using a ubuntu based distro (such as Mint), in step 4 of the ubuntu/debian install replace  ```$(lsb_release -cs) \``` with the actual name of the ubuntu/debian distro your OS is based upon (For [Mint Tessa](https://linuxmint.com/download_all.php), it would be bionic, for [bunsen hydrogen](https://www.bunsenlabs.org/repositories.html), it would be jessie)
* docker-compose

Installation
--------------
```
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/lzbk/MagicWord.git
cd MagicWord
cp .env.dist .env
vi .env
#vi or any other text editor, this will allow you to set the DB name & password
#these informations will be asked during the install
sudo docker-compose up --build -d
sudo docker-compose exec apache bash
composer self-update 2.2.9 
make install
```

Notes
-----
* During the last docker command, you should be able to leave the default value for every parameter by pressing ```enter```, except for:
  * the ```database_password``` which should match the one you entered in .env file and in the PWD var
  * the ```secret``` phrase, which should be changed
* if you let default values, MW should be available at localhost:666 and the adminer at localhost:667
* You'd better leave default values, some stuff is hardcoded...


Import lexicon
------
copy `lexicon.tsv` and `spec.txt` in a new folder in `application/data/lexicons`

(some ready to use scripts are available in the `script/` folder.)
```
sudo docker-compose exec apache bash
console lexicon:import
```

Grid generation
------
Once application installed, you can generate some grids with this command in the apache container. It will generates 10 grids with at least 150 foundable forms. (you will be prompted to enter a lexicon ID)
```
php bin/console magicword:generate-grid 10 150 0
```

Update
------
```
git pull origin master
sudo docker-compose up --build -d
sudo docker-compose exec apache bash
make update
```

Create Administrator
--------------------
To have an administrator of your Magic Word instance,
1. you first need to create an account on the site, like any other account.
2. then launch php container `sudo docker-compose exec apache bash`
3. in that container type `php bin/console fos:user:promote`
1. enter the screen name of the account to promote
1. submit the role `ROLE_ADMIN`
