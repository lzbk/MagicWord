<?php

namespace LexiconBundle\Command;

use LexiconBundle\Manager\ImportManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Finder\Finder;

class ImportLexiconCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
          ->setName('lexicon:import')
          ->setDescription('Put your files, named exactly "lexicon.tsv" & "spec.txt" in a directory (ex:/euskara) in application/data/lexicons');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Available directories in <comment>./data/lexicons/</comment>');
        $finder = new Finder();
        $dirs = $finder->directories()->in('./data/lexicons')->depth('== 0');
        $directories = [];
        foreach ($dirs as $dir) {
            $name = basename($dir);
            $directories[] = $name;
            $output->writeln('* <comment>'.$name.'</comment>');
        }

        $helper = $this->getHelper('question');
        $dirName = new Question("Please enter the directory name \n");
        $pathLexiconDir = $helper->ask($input, $output, $dirName);

        if (in_array($pathLexiconDir, $directories)) {
            echo 'You passed an argument: ' . $pathLexiconDir . "\n\n";
            $importManager = $this->getContainer()->get('lexicon_manager.import');
            $importManager->importLexicon($pathLexiconDir);
        }
    }
}
