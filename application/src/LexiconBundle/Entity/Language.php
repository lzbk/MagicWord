<?php

namespace LexiconBundle\Entity;

use MagicWordBundle\Entity\Rules\ComboPoints;
use MagicWordBundle\Entity\Rules\WordLength;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="lexicon_language")
 * @ORM\Entity()
 */
class Language
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @var int
     *
     * @ORM\Column(name="minBigram", type="integer")
     */
    private $minBigram = 1;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tagset;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $relationType;

    /**
     * @ORM\OneToMany(targetEntity="Feature", mappedBy="language", cascade={"remove"})
     */
    private $features;

    /**
     * @ORM\OneToMany(targetEntity="Letter", mappedBy="language", cascade={"remove"})
     */
    private $letters;

    /**
     * @ORM\OneToMany(targetEntity="MagicWordBundle\Entity\Rules\WordLength", mappedBy="language", cascade={"remove"})
     */
    private $wordLengths;

    /**
     * @ORM\OneToMany(targetEntity="MagicWordBundle\Entity\Rules\ComboPoints", mappedBy="language", cascade={"remove"})
     */
    private $comboPoints;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $directory;


    public function __construct()
    {
        $this->features = new ArrayCollection();
        $this->letters = new ArrayCollection();
        $this->wordLengths = new ArrayCollection();
        $this->comboPoints = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getTagset(): ?string
    {
        return $this->tagset;
    }

    public function setTagset(?string $tagset): self
    {
        $this->tagset = $tagset;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getRelationType(): ?string
    {
        return $this->relationType;
    }

    public function setRelationType(string $relationType): self
    {
        $this->relationType = $relationType;

        return $this;
    }

    /**
     * @return Collection|Feature[]
     */
    public function getFeatures(): Collection
    {
        return $this->features;
    }

    public function addFeature(Feature $feature): self
    {
        if (!$this->features->contains($feature)) {
            $this->features[] = $feature;
            $feature->setLanguage($this);
        }

        return $this;
    }

    public function removeFeature(Feature $feature): self
    {
        if ($this->features->contains($feature)) {
            $this->features->removeElement($feature);
            // set the owning side to null (unless already changed)
            if ($feature->getLanguage() === $this) {
                $feature->setLanguage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Letter[]
     */
    public function getLetters(): Collection
    {
        return $this->letters;
    }

    public function addLetter(Letter $letter): self
    {
        if (!$this->letters->contains($letter)) {
            $this->letters[] = $letter;
            $letter->setLanguage($this);
        }

        return $this;
    }

    public function removeLetter(Letter $letter): self
    {
        if ($this->letters->contains($letter)) {
            $this->letters->removeElement($letter);
            // set the owning side to null (unless already changed)
            if ($letter->getLanguage() === $this) {
                $letter->setLanguage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|WordLength[]
     */
    public function getWordLengths(): Collection
    {
        return $this->wordLengths;
    }

    public function addWordLength(WordLength $wordLength): self
    {
        if (!$this->wordLengths->contains($wordLength)) {
            $this->wordLengths[] = $wordLength;
            $wordLength->setLanguage($this);
        }

        return $this;
    }

    public function removeWordLength(WordLength $wordLength): self
    {
        if ($this->wordLengths->contains($wordLength)) {
            $this->wordLengths->removeElement($wordLength);
            // set the owning side to null (unless already changed)
            if ($wordLength->getLanguage() === $this) {
                $wordLength->setLanguage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ComboPoints[]
     */
    public function getComboPoints(): Collection
    {
        return $this->comboPoints;
    }

    public function addComboPoints(ComboPoints $comboPoints): self
    {
        if (!$this->comboPoints->contains($comboPoints)) {
            $this->comboPoints[] = $comboPoints;
            $comboPoints->setLanguage($this);
        }

        return $this;
    }

    public function removeComboPoints(ComboPoints $comboPoints): self
    {
        if ($this->comboPoints->contains($comboPoints)) {
            $this->comboPoints->removeElement($comboPoints);
            // set the owning side to null (unless already changed)
            if ($comboPoints->getLanguage() === $this) {
                $comboPoints->setLanguage(null);
            }
        }

        return $this;
    }


    public function getDirectory(): ?string
    {
        return $this->directory;
    }

    public function setDirectory(string $directory): self
    {
        $this->directory = $directory;

        return $this;
    }


    /**
     * Add comboPoint.
     *
     * @param \MagicWordBundle\Entity\Rules\ComboPoints $comboPoint
     *
     * @return Language
     */
    public function addComboPoint(\MagicWordBundle\Entity\Rules\ComboPoints $comboPoint)
    {
        $this->comboPoints[] = $comboPoint;

        return $this;
    }

    /**
     * Remove comboPoint.
     *
     * @param \MagicWordBundle\Entity\Rules\ComboPoints $comboPoint
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeComboPoint(\MagicWordBundle\Entity\Rules\ComboPoints $comboPoint)
    {
        return $this->comboPoints->removeElement($comboPoint);
    }

    /**
     * Set minBigram.
     *
     * @param int $minBigram
     *
     * @return Language
     */
    public function setMinBigram($minBigram)
    {
        $this->minBigram = $minBigram;

        return $this;
    }

    /**
     * Get minBigram.
     *
     * @return int
     */
    public function getMinBigram()
    {
        return $this->minBigram;
    }
}
