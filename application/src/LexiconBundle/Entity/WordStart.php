<?php

namespace LexiconBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Table(name="lexicon_word_start", indexes={
 *  @Index(columns={"value"}, flags={"fulltext"}),
 *  @Index(columns={"value"}, name="value"),
 *  @Index(name="language", columns={"language"}),
 * })
 * @ORM\Entity(repositoryClass="LexiconBundle\Repository\WordStartRepository")
 */
class WordStart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $language;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLanguage(): ?int
    {
        return $this->language;
    }

    public function setLanguage(int $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
