<?php

namespace LexiconBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Table(name="lexicon_word", indexes={
 *  @Index(columns={"clean_value"}, name="cleanValue"),
 *  @Index(name="language", columns={"language_id"}),
 * }))
 * @ORM\Entity(repositoryClass="LexiconBundle\Repository\WordRepository")
 */
class Word implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cleanValue;

    /**
     * @ORM\ManyToMany(targetEntity="Feature", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $features;

    /**
     * @ORM\ManyToOne(targetEntity="Root", inversedBy="words")
     */
    private $root;



    public function __construct()
    {
        $this->features = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getCleanValue(): ?string
    {
        return $this->cleanValue;
    }

    public function setCleanValue(string $cleanValue): self
    {
        $this->cleanValue = $cleanValue;

        return $this;
    }

    /**
     * @return Collection|Feature[]
     */
    public function getFeatures(): Collection
    {
        return $this->features;
    }

    public function addFeature(Feature $feature): self
    {
        if (!$this->features->contains($feature)) {
            $this->features[] = $feature;
        }

        return $this;
    }

    public function removeFeature(Feature $feature): self
    {
        if ($this->features->contains($feature)) {
            $this->features->removeElement($feature);
        }

        return $this;
    }

    public function getRoot(): ?Root
    {
        return $this->root;
    }

    public function setRoot(?Root $root): self
    {
        $this->root = $root;

        return $this;
    }


    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'value' => $this->value,
            'cleanValue' => $this->cleanValue,
        );
    }
}
