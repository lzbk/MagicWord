<?php

namespace LexiconBundle\Repository;

use LexiconBundle\Entity\Word;
use LexiconBundle\Entity\Language;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class WordRepository extends \Doctrine\ORM\EntityRepository
{
    public function getExistingWords($words, $language)
    {
        $em = $this->_em;

        $dql = 'SELECT w FROM LexiconBundle\Entity\Word w
              WHERE w.language = :language
              AND w.cleanValue IN (:words)';

        $query = $em->createQuery($dql);
        $query->setParameter('language', $language);
        $query->setParameter('words', $words);

        $results = $query->getResult();

        return $results;
    }

    public function deleteByLanguage(Language $language)
    {
        return $this->createQueryBuilder('w')
            ->delete(Word::class, 'w')
            ->andWhere('w.language = :val')
            ->setParameter('val', $language)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getByLanguageAndContentOrCleaned($language, $value)
    {
        $em = $this->_em;
        $dql = 'SELECT w FROM LexiconBundle\Entity\Word w
              WHERE w.language = :language
              AND (w.value = :value OR w.cleanValue = :value)';

        $query = $em->createQuery($dql);
        $query->setParameter('language', $language)
              ->setParameter('value', $value)
              ->setMaxResults(1);

        return $query->getOneOrNullResult();
    }

    public function countAllByLanguage(Language $language)
    {
        $em = $this->_em;
        $dql = "SELECT count(w.id) FROM LexiconBundle\Entity\Word w
              WHERE w.language = :language";

        $query = $em->createQuery($dql);
        $query->setParameter('language', $language);

        return $query->getSingleScalarResult();
    }
}
