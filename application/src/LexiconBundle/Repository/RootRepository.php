<?php

namespace LexiconBundle\Repository;

use LexiconBundle\Entity\Language;
use LexiconBundle\Entity\Root;
use Symfony\Bridge\Doctrine\RegistryInterface;

class RootRepository extends \Doctrine\ORM\EntityRepository
{
    public function deleteByLanguage(Language $language)
    {
        return $this->createQueryBuilder('r')
            ->delete(Root::class, 'r')
            ->andWhere('r.language = :val')
            ->setParameter('val', $language)
            ->getQuery()
            ->getResult()
        ;
    }

    public function countAllByLanguage(Language $language)
    {
        $em = $this->_em;
        $dql = "SELECT count(r.id) FROM LexiconBundle\Entity\Root r
              WHERE r.language = :language";

        $query = $em->createQuery($dql);
        $query->setParameter('language', $language);

        return $query->getSingleScalarResult();
    }

    public function getByContentAndLanguage($findWord)
    {
        $em = $this->_em;

        $dql = 'SELECT r FROM LexiconBundle\Entity\Root r
                WHERE EXISTS(
                    SELECT w FROM LexiconBundle\Entity\Word w
                    WHERE w.root = r
                    AND w.cleanValue = :cleanValue
                    AND w.language = :language
                )';

        $query = $em->createQuery($dql);
        $query->setParameter('cleanValue', $findWord->getInflection());
        $query->setParameter('language', $findWord->getConquer()->getLanguage());

        return $query->getResult();
    }
}
