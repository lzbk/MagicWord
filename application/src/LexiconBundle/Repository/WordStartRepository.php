<?php

namespace LexiconBundle\Repository;

use LexiconBundle\Entity\WordStart;
use LexiconBundle\Entity\Language;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class WordStartRepository extends \Doctrine\ORM\EntityRepository
{
    public function deleteByLanguage($language)
    {
        return $this->createQueryBuilder('ws')
            ->delete(WordStart::class, 'ws')
            ->andWhere('ws.language = :val')
            ->setParameter('val', $language)
            ->getQuery()
            ->getResult();
    }

    public function countAllByLanguage(Language $language)
    {
        $em = $this->_em;
        $dql = "SELECT count(w.id) FROM LexiconBundle\Entity\WordStart w
              WHERE w.language = :language";

        $query = $em->createQuery($dql);
        $query->setParameter('language', $language);

        return $query->getSingleScalarResult();
    }
}
