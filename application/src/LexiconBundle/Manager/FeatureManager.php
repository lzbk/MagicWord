<?php

namespace LexiconBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use LexiconBundle\Entity\Feature;
use LexiconBundle\Entity\Language;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("lexicon_manager.feature")
 */
class FeatureManager
{
    private $em;

    /**
     * @DI\InjectParams({
     *      "em" = @DI\Inject("doctrine.orm.entity_manager"),
     * })
     */
    public function __construct($em)
    {
        $this->em = $em;
    }

    public function findOrCreate(Language $language, $label, $value, &$featuresToFlush)
    {
        if (!array_key_exists($label, $featuresToFlush) || !array_key_exists($value, $featuresToFlush[$label])) {
            if (!$feature = $this->em->getRepository(Feature::class)->findOneBy(['value' => $value, 'label' => $label, 'language' => $language])) {
                $feature = new Feature();
                $feature->setLanguage($language);
                $feature->setLabel($label);
                $feature->setValue($value);

                $this->em->persist($feature);

                $featuresToFlush[$label][$value] = $feature;
            }
            return $feature;
        }

        return $featuresToFlush[$label][$value];
    }
}
