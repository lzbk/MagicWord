<?php

namespace LexiconBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use LexiconBundle\Entity\Letter;
use LexiconBundle\Entity\Language;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("lexicon_manager.letter")
 */
class LetterManager
{
    private $em;

    /**
     * @DI\InjectParams({
     *      "em" = @DI\Inject("doctrine.orm.entity_manager"),
     * })
     */
    public function __construct($em)
    {
        $this->em = $em;
    }

    public function create(Language $language, $letters, $weights)
    {
        $string2print="Lettre Language= ".$language->getValue()."\n";
        foreach ($letters as $letterValue) {
            // if (ctype_alnum($letterValue)) {
            $letter = new Letter();
            $letter->setLanguage($language);
            $letter->setValue($letterValue);
            $letter->setPoints(isset($weights[$letterValue]) ? $weights[$letterValue] : 1);
            // $string2print="Lettre OK ctype_alpha = ".$letterValue."\n";

            $this->em->persist($letter);
            // }
        }
        return;
    }
}
