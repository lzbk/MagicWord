<?php

namespace MagicWordBundle\Controller\Lexicon;

use LexiconBundle\Entity\Language;
use MagicWordBundle\Entity\Lexicon\Word;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class WordController extends Controller
{
    /**
     * @Route("/word/{id}", name="word", requirements={"id": "\d+"})
     */
    public function getInfosAction(Word $word)
    {
        return $this->render('MagicWordBundle:Lexicon:word.html.twig', ['word' => $word]);
    }

    /**
     * @Route("/word/check/{id}", name="check_existence", options={"expose"=true})
     * @Method("POST")
     */
    public function checkExistenceAction(Language $language, Request $request)
    {
        $word = $request->request->get('word');
        $word = $this->get('lexicon_manager.word')->checkExistence($word, $language);

        return new JsonResponse($word);
    }
}
