<?php

namespace MagicWordBundle\Controller;

use LexiconBundle\Entity\Language;
use LexiconBundle\Entity\Root;
use MagicWordBundle\Entity\Player;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class WordboxController extends Controller
{
    /**
     * @Route("/wordbox/{id}", name ="wordbox")
     */
    public function displayWordboxAction(Language $language)
    {
        $acquisitions = $this->get('mw_manager.wordbox')->getAcquisitionsByLanguage($language);

        return $this->render('MagicWordBundle:Wordbox:index.html.twig', [
            'acquisitions' => $acquisitions,
            'language' => $language,
        ]);
    }

    /**
     * @Route("/wordbox/{playerId}/{languageId}", name ="wordbox_user")
     * @ParamConverter("player", class="MagicWordBundle:Player",  options={"id" = "playerId"})
     * @ParamConverter("language", class="LexiconBundle:Language", options={"id" = "languageId"})
     */
    public function displaySomeoneWordboxAction(Player $player, Language $language)
    {
        $acquisitions = $this->get('mw_manager.wordbox')->getAcquisitionsByLanguage($language, $player);

        return $this->render('MagicWordBundle:Wordbox:index.html.twig', [
            'acquisitions' => $acquisitions,
            'language' => $language,
            'player' => $player
        ]);
    }

    /**
     * @Route("/wordbox/add/{id}", name="add-to-wordbox")
     * @ParamConverter("root", class="LexiconBundle:Root")
     */
    public function addToWordboxAction(Root $root)
    {
        $this->get('mw_manager.wordbox')->addToWordbox($root, 'manual');

        return $this->redirectToRoute('wordbox');
    }

    /**
     * @Route("/js/wordbox/add/{id}", name="add-to-wordbox-js", options={"expose"=true})
     * @ParamConverter("root", class="LexiconBundle:Root")
     */
    public function addToWordboxJSAction(Root $root)
    {
        $this->get('mw_manager.wordbox')->addToWordbox($root, 'manual');

        return new Response();
    }
}
