<?php

namespace MagicWordBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class WiktionnaryController extends Controller
{
    /**
     * @Route("/wiktionnary/{lemma}/{language}", name="wiktionnary", options={"expose"=true})
     * @Method("GET")
     */
    public function getWiktionnaryDefAction($lemma, $language)
    {
        $def = $this->get('wiktionary')->getDefinitions($lemma, $language);

        return new Response($def);
    }
}
