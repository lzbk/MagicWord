<?php

namespace  MagicWordBundle\Manager;

use JMS\DiExtraBundle\Annotation as DI;
use MagicWordBundle\Entity\FoundableForm;
use MagicWordBundle\Entity\Grid;

/**
 * @DI\Service("mw_manager.foundableform")
 */
class FoundableFormManager
{
    protected $em;
    protected $scoreManager;
    protected $wordManager;

    /**
     * @DI\InjectParams({
     *      "entityManager" = @DI\Inject("doctrine.orm.entity_manager"),
     *      "scoreManager" = @DI\Inject("mw_manager.score"),
     *      "wordManager" = @DI\Inject("lexicon_manager.word")
     * })
     */
    public function __construct($entityManager, $scoreManager, $wordManager)
    {
        $this->em = $entityManager;
        $this->scoreManager = $scoreManager;
        $this->wordManager = $wordManager;
    }

    public function populateFoundables($inflections, Grid $grid)
    {
        $foundableForms = [];

        foreach ($inflections as $inflection) {
            if (!isset($foundableForms[$inflection->getCleanValue()])) {
                $foundableForms[$inflection->getCleanValue()] = ['inflections' => [$inflection]];
            } else {
                $foundableForms[$inflection->getCleanValue()]['inflections'][] = $inflection;
            }
        }

        foreach ($foundableForms as $form => $foundableForm) {
            $foundable = new FoundableForm();
            $foundable->setGrid($grid);
            $foundable->setForm($form);
            foreach ($foundableForm['inflections'] as $inflection) {
                $foundable->addWord($inflection);
            }
            $points = $this->wordManager->recalculate($foundable->getWords()[0]);
            $foundable->setPoints($points);

            $this->em->persist($foundable);
            $grid->addFoundableForm($foundable);
        }

        $grid->setFoundableCount(count($foundableForms));

        $this->em->persist($grid);
        $this->em->flush();

        return;
    }
}
