<?php

namespace  MagicWordBundle\Manager;

use JMS\DiExtraBundle\Annotation as DI;
use MagicWordBundle\Entity\GeneralParameters;
use MagicWordBundle\Form\Type\GeneralParametersType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @DI\Service("mw_manager.administration")
 */
class AdministrationManager
{
    protected $em;
    protected $formFactory;

    /**
     * @DI\InjectParams({
     *      "entityManager" = @DI\Inject("doctrine.orm.entity_manager"),
     *      "formFactory"   = @DI\Inject("form.factory"),
     * })
     */
    public function __construct($entityManager, $formFactory)
    {
        $this->em = $entityManager;
        $this->formFactory = $formFactory;
    }

    public function getGeneralParametersForm()
    {
        $generalParameters = $this->getGeneralParameters();

        if (!$generalParameters) {
            $generalParameters = new GeneralParameters();
            $generalParameters->setHomeText("lorem ipsum...");
            $generalParameters->setSelfRegistration(true);
            $this->em->persist($generalParameters);
            $this->em->flush();
        }

        $form = $this->formFactory->createBuilder(GeneralParametersType::class, $generalParameters)->getForm()->createView();

        return $form;
    }

    public function handleGeneralParametersForm(Request $request)
    {
        $generalParameters = $this->getGeneralParameters();

        $form = $this->formFactory->createBuilder(GeneralParametersType::class, $generalParameters)->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->em->persist($generalParameters);
            $this->em->flush();
        }

        return;
    }

    public function getGeneralParameters()
    {
        $generalParameters = $this->em->getRepository("MagicWordBundle:GeneralParameters")->get();

        return $generalParameters;
    }
}
