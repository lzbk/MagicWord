<?php

namespace  MagicWordBundle\Manager;

use JMS\DiExtraBundle\Annotation as DI;
use MagicWordBundle\Entity\Square;

/**
 * @DI\Service("mw_manager.square")
 */
class SquareManager
{
    protected $em;

    /**
     * @DI\InjectParams({
     *      "entityManager" = @DI\Inject("doctrine.orm.entity_manager"),
     * })
     */
    public function __construct($entityManager)
    {
        $this->em = $entityManager;
    }

    public function create($letter, $grid)
    {
        $language = $grid->getLanguage();
        $letter = $this->em->getRepository("LexiconBundle:Letter")->findOneBy(["value" => $letter, "language" => $language]);

        $square = new Square();
        $square->setLetter($letter);
        $square->setGrid($grid);
        $this->em->persist($square);
        $this->em->persist($grid);
        $this->em->flush();

        return $square;
    }
}
