<?php

namespace MagicWordBundle\Repository\Wordbox;

use MagicWordBundle\Entity\Wordbox;
use MagicWordBundle\Entity\Wordbox\Acquisition;
use LexiconBundle\Entity\Language;

class AcquisitionRepository extends \Doctrine\ORM\EntityRepository
{
    public function findOneByWordboxAndLanguage(Wordbox $wordbox, Language $language)
    {
        $em = $this->_em;
        $dql = "SELECT acquisition FROM MagicWordBundle\Entity\Wordbox\Acquisition acquisition
                LEFT JOIN acquisition.root root
                WHERE root.language = :language
                AND acquisition.wordbox = :wordbox
                ORDER BY root.value ASC
                ";

        $query = $em->createQuery($dql);
        $query->setParameter('wordbox', $wordbox)
              ->setParameter('language', $language);

        return $query->getResult();
    }

    public function deleteByLanguage(Language $language)
    {
        return $this->createQueryBuilder('a')
          ->delete(Acquisition::class, 'a')
          ->join('a.root', 'r')
          ->join('r.language', 'l', 'WITH', 'l.id = ?1', $language->getId())
          ->getQuery()
          ->getResult()
      ;
    }
}
