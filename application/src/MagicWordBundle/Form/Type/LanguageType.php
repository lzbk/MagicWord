<?php

namespace MagicWordBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class LanguageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('value', TextType::class, [
          'attr' => array('class' => 'form-control'),
          'label' => 'name',
        ]);

        $builder->add('minBigram', IntegerType::class, [
            'attr' => array('class' => 'form-control'),
            'label' => 'minBigram',
        ]);

        $builder->add('save', SubmitType::class, array(
            'attr' => array('class' => 'btn btn-secondary btn-sm mt-2'),
            'label' => 'save',
            'translation_domain' => 'messages',
        ));

        $builder->setMethod('POST');
    }

    public function getName()
    {
        return 'language_parameters';
    }
}
