<?php

namespace MagicWordBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LetterLanguagePointsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('points', IntegerType::class, array(
            'attr' => [
                'placeholder' => 'points',
                'translation_domain' => 'messages',
            ],
            'label' => 'points',
        ));

        $builder->add('save', SubmitType::class, array(
            'attr' => array('class' => 'btn btn-secondary btn-sm mt-2'),
            'label' => 'save',
            'translation_domain' => 'messages',
        ));
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LexiconBundle\Entity\Letter',
        ));
    }
}
