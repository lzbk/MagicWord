<?php

namespace MagicWordBundle\Form\Type\ObjectiveType;

use MagicWordBundle\Form\Type\ObjectiveType\FeatureType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use LexiconBundle\Entity\Feature;

class ConstraintType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $languageId = $options["languageId"];

        $builder->add('numberToFind', IntegerType::class, [
            'label' => 'constraint_number',
            'translation_domain' => 'messages',
            'attr' => [
                'class' => 'form-control',
                'data-field' => 'number',
                'maxlength' => '3',
                'min' => 1,
                'step' => 1
            ],
        ]);

        $builder->add('features', EntityType::class, [
            'choice_label' => 'value',
            'class' => Feature::class,
            'query_builder' => function (EntityRepository $repo) use ($languageId) {
                $qb = $repo->createQueryBuilder('f');
                $qb->andWhere('f.language = '.$languageId);
                $qb->orderBy('f.value', 'ASC');

                return $qb;
            },
            'group_by' => 'label',
            'multiple' => true,
            'placeholder' => '- feature -',
            'required' => true,
            'empty_data' => null,
            'attr' => [
                'class' => 'form-control'
            ]
        ]);
    }

    public function getName()
    {
        return 'constraint';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'languageId' => null,
            'data_class' => 'MagicWordBundle\Entity\ObjectiveType\Constraint'
        ]);
    }
}
