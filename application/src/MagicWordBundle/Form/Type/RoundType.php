<?php

namespace MagicWordBundle\Form\Type;

use MagicWordBundle\Form\Type\ObjectiveType\ComboType;
use MagicWordBundle\Form\Type\ObjectiveType\ConstraintType;
use MagicWordBundle\Form\Type\ObjectiveType\FindWordType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoundType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $languageId = $options["languageId"];

        $builder->add('findWords', CollectionType::class, array(
            'entry_type' => FindWordType::class,
            'prototype' => true,
            'allow_add' => true,
            'allow_delete' => true,
            'mapped' => true,
            'by_reference' => false,
        ));

        $builder->add('constraints', CollectionType::class, [
            'entry_type' => ConstraintType::class,
            'prototype' => true,
            'allow_add' => true,
            'allow_delete' => true,
            'mapped' => true,
            'by_reference' => false,
            'prototype_name' => '__constraint_prot__',
            'entry_options'  => [
                'languageId'  => $languageId
            ],
        ]);

        $builder->add('combos', CollectionType::class, array(
            'entry_type' => ComboType::class,
            'prototype' => true,
            'allow_add' => true,
            'allow_delete' => true,
            'mapped' => true,
            'by_reference' => false,
        ));
    }

    public function getName()
    {
        return 'conquer';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
          'languageId' => null
        ]);
    }
}
