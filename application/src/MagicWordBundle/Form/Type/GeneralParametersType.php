<?php

namespace MagicWordBundle\Form\Type;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class GeneralParametersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('homeText', CKEditorType::class, [
          'config' => array('toolbar' => 'basic'),
          'attr' => array('class' => 'form-control'),
          'label' => 'homeText',
        ]);

        $builder->add('footer', CKEditorType::class, [
            'config' => [
              'toolbar' => 'basic',
              'autoParagraph' => false
            ],
            'attr' => ['class' => 'form-control'],
            'label' => 'footer',
            'required' => false,
        ]);

        $builder->add('selfRegistration', CheckboxType::class, array(
            'label' => 'selfRegistration',
            'required' => false,
            'translation_domain' => 'messages',
        ));

        $builder->add('tutorial', EntityType::class, array(
          'class' => 'MagicWordBundle:Game',
          'choice_label' => 'id',
          'attr' => array('class' => 'form-control'),
          'label' => 'tutorial_game_id',
          'translation_domain' => 'messages',
          'choice_translation_domain' => 'messages',
        ));


        $builder->add('save', SubmitType::class, array(
            'attr' => array('class' => 'btn btn-secondary btn-sm mt-2'),
            'label' => 'save',
            'translation_domain' => 'messages',
        ));

        $builder->setMethod('POST');
    }

    public function getName()
    {
        return 'general_parameters';
    }
}
