<?php

namespace MagicWordBundle\Entity\ObjectiveType;

use Doctrine\ORM\Mapping as ORM;
use MagicWordBundle\Entity\Objective;

/**
 * FindWord.
 *
 * @ORM\Table(name="objective_type_findword")
 * @ORM\Entity()
 */
class FindWord extends Objective
{
    protected $discr = 'findword';

    public function getDiscr()
    {
        return $this->discr;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="hint", type="text", nullable=false)
     */
    protected $hint;

    /**
     * @var string
     *
     * @ORM\Column(name="inflection", type="text", nullable=false)
     */
    protected $inflection;

    /**
     * @ORM\ManyToMany(targetEntity="LexiconBundle\Entity\Root")
     * @ORM\JoinTable(name="objective_findword_root")
     */
    private $roots;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $lemmaEnough;

    /**
     * Set hint.
     *
     * @param string $hint
     *
     * @return FindWord
     */
    public function setHint($hint)
    {
        $this->hint = $hint;

        return $this;
    }

    /**
     * Get hint.
     *
     * @return string
     */
    public function getHint()
    {
        return $this->hint;
    }

    /**
     * Set inflection.
     *
     * @param string $inflection
     *
     * @return FindWord
     */
    public function setInflection($inflection)
    {
        $this->inflection = $inflection;

        return $this;
    }

    /**
     * Get inflection.
     *
     * @return string
     */
    public function getInflection()
    {
        return $this->inflection;
    }

    /**
     * Set lemmaEnough.
     *
     * @param bool $lemmaEnough
     *
     * @return FindWord
     */
    public function setLemmaEnough($lemmaEnough)
    {
        $this->lemmaEnough = $lemmaEnough;

        return $this;
    }

    /**
     * Get lemmaEnough.
     *
     * @return bool
     */
    public function getLemmaEnough()
    {
        return $this->lemmaEnough;
    }
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->lemmas = new \Doctrine\Common\Collections\ArrayCollection();
    }


    public function export()
    {
        $jsonArray = array(
            'type' => $this->discr,
            'inflection' => $this->inflection,
            'hint' => $this->hint,
            'lemmaEnough' => $this->lemmaEnough,
        );

        return $jsonArray;
    }

    /**
     * Add root.
     *
     * @param \LexiconBundle\Entity\Root $root
     *
     * @return FindWord
     */
    public function addRoot(\LexiconBundle\Entity\Root $root)
    {
        $this->roots[] = $root;

        return $this;
    }

    /**
     * Add lemmas.
     *
     * @param \MagicWordBundle\Entity\Lexicon\Lemma $lemma
     *
     * @return FindWord
     */
    public function addRoots($roots)
    {
        foreach ($roots as $root) {
            $this->roots[] = $root;
        }

        return $this;
    }


    /**
     * Remove root.
     *
     * @param \LexiconBundle\Entity\Root $root
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeRoot(\LexiconBundle\Entity\Root $root)
    {
        return $this->roots->removeElement($root);
    }

    /**
     * Get roots.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoots()
    {
        return $this->roots;
    }
}
