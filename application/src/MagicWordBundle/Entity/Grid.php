<?php

namespace MagicWordBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Grid.
 *
 * @ORM\Table(name="grid")
 * @ORM\Entity(repositoryClass="MagicWordBundle\Repository\GridRepository")
 */
class Grid implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="side", type="integer")
     */
    private $side = 4;

    /**
     * @var int
     *
     * @ORM\Column(name="foundableCount", type="integer")
     */
    private $foundableCount = 0;

    /**
     * @ORM\OneToMany(targetEntity="FoundableForm", mappedBy="grid", cascade={"remove"})
     * @ORM\OrderBy({"points" = "DESC"})
     */
    private $foundableForms;

    /**
     * @ORM\OneToMany(targetEntity="Square", mappedBy="grid", cascade={"remove"})
     */
    protected $squares;

    /**
     * @ORM\ManyToOne(targetEntity="LexiconBundle\Entity\Language")
     */
    private $language;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $json;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set language.
     *
     * @param \LexiconBundle\Entity\Language $language
     *
     * @return Game
     */
    public function setLanguage(\LexiconBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language.
     *
     * @return \LexiconBundle\Entity\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    public function jsonSerialize()
    {
        $jsonArray = [
            'id' => $this->id,
            'inflections' => [],
        ];
        foreach ($this->getFoundableForms() as $foundable) {
            $form = $foundable->getForm();
            $jsonArray['inflections'][$form] =  [
              'id' => $foundable->getId(),
              'points' => $foundable->getPoints(),
              'features' => [],
              'ids' => [],
              'rootIds' => []
            ];
            foreach ($foundable->getWords() as $word) {
                $root = $word->getRoot();
                foreach ($word->getFeatures() as $feature) {
                    $jsonArray['inflections'][$form]['features'][] = $feature->getId();
                }
                $jsonArray['inflections'][$form]['ids'][] = $word->getId();
                $rootId = $root->getId();
                if (!in_array($rootId, $jsonArray['inflections'][$form]['rootIds'])) {
                    $jsonArray['inflections'][$form]['rootIds'][] = $rootId;
                }
            }
        }

        return $jsonArray;
    }

    /**
     * Add foundableForm.
     *
     * @param \MagicWordBundle\Entity\FoundableForm $foundableForm
     *
     * @return Grid
     */
    public function addFoundableForm(\MagicWordBundle\Entity\FoundableForm $foundableForm)
    {
        $this->foundableForms[] = $foundableForm;

        return $this;
    }

    /**
     * Remove foundableForm.
     *
     * @param \MagicWordBundle\Entity\FoundableForm $foundableForm
     */
    public function removeFoundableForm(\MagicWordBundle\Entity\FoundableForm $foundableForm)
    {
        $this->foundableForms->removeElement($foundableForm);
    }

    /**
     * Get foundableForms.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFoundableForms()
    {
        return $this->foundableForms;
    }

    /**
     * Set json
     *
     * @param string $json
     *
     * @return Grid
     */
    public function setJson($json)
    {
        $this->json = $json;

        return $this;
    }

    /**
     * Get json
     *
     * @return string
     */
    public function getJson()
    {
        return $this->json;
    }

    /**
     * Set side.
     *
     * @param int $side
     *
     * @return Grid
     */
    public function setSide($side)
    {
        $this->side = $side;

        return $this;
    }

    /**
     * Get side.
     *
     * @return int
     */
    public function getSide()
    {
        return $this->side;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->foundableForms = new \Doctrine\Common\Collections\ArrayCollection();
        $this->squares = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add square.
     *
     * @param \MagicWordBundle\Entity\Square $square
     *
     * @return Grid
     */
    public function addSquare(\MagicWordBundle\Entity\Square $square)
    {
        $this->squares[] = $square;

        return $this;
    }

    /**
     * Remove square.
     *
     * @param \MagicWordBundle\Entity\Square $square
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSquare(\MagicWordBundle\Entity\Square $square)
    {
        return $this->squares->removeElement($square);
    }

    /**
     * Get squares.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSquares()
    {
        return $this->squares;
    }

    /**
     * Set foundableCount.
     *
     * @param int $foundableCount
     *
     * @return Grid
     */
    public function setFoundableCount($foundableCount)
    {
        $this->foundableCount = $foundableCount;

        return $this;
    }

    /**
     * Get foundableCount.
     *
     * @return int
     */
    public function getFoundableCount()
    {
        return $this->foundableCount;
    }
}
