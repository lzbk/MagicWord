<?php

namespace MagicWordBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Square.
 *
 * @ORM\Table(name="square")
 * @ORM\Entity()
 */
class Square
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="LexiconBundle\Entity\Letter")
     */
    protected $letter;

    /**
     * @ORM\ManyToOne(targetEntity="Grid", inversedBy="squares")
     */
    private $grid;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set letter.
     *
     * @param \LexiconBundle\Entity\Letter|null $letter
     *
     * @return Square
     */
    public function setLetter(\LexiconBundle\Entity\Letter $letter = null)
    {
        $this->letter = $letter;

        return $this;
    }

    /**
     * Get letter.
     *
     * @return \LexiconBundle\Entity\Letter|null
     */
    public function getLetter()
    {
        return $this->letter;
    }

    /**
     * Set grid.
     *
     */
    public function setGrid(\MagicWordBundle\Entity\Grid $grid = null)
    {
        $this->grid = $grid;

        return $this;
    }

    /**
     * Get grid.
     *
     */
    public function getGrid()
    {
        return $this->grid;
    }
}
