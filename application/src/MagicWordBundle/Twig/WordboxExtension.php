<?php

namespace  MagicWordBundle\Twig;

use LexiconBundle\Entity\Root;

class WordboxExtension extends \Twig_Extension
{
    protected $em;
    protected $wordboxManager;

    public function __construct($entityManager, $wordboxManager)
    {
        $this->em = $entityManager;
        $this->wordboxManager = $wordboxManager;
    }

    public function isInWordbox(Root $root)
    {
        $inWordbox = $this->wordboxManager->isInWordbox($root);

        return $inWordbox;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('isInWordbox', array($this, 'isInWordbox')),
        );
    }

    public function getName()
    {
        return 'wordbox';
    }
}
