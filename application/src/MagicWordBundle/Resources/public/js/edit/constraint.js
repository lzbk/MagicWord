var constraints = {
    add: function(){
        var prototype = $("#prototypes").data("prototype-constraint");
        var constraints = $('#constraints');
        var constraint = prototype;
        var constraintObjectives = document.getElementsByClassName('constraint-objective');
        var length = constraintObjectives.length;
        constraint = constraint.replace(/__constraint_prot__/g, length);
        constraints.append(constraint);
    },
    checkAll: function(){
        $( "#constraints-tab select" ).each(function() {
            constraints.checkOne($(this));
        });
    },
    checkOne: function(constraint){
        var span = constraint.parent("span");
        if (constraint.val() != "") {
            span.addClass("notempty");
        } else{
            span.removeClass("notempty");
        }
    }
};

$( "#constraints-tab" ).on( "change", "select", function(){
    constraints.checkOne($(this));
});

$( "#constraints-tab" ).ready(function() {
    constraints.checkAll();
});
