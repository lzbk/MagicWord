var wiktionnary = {
    modaleGenerated: false,
    getDef: function(lemma, language){
        var url = Routing.generate('wiktionnary', {lemma: lemma, language: language});
        $('#wiktionnary-body').empty();
        $.ajax({
            type: "GET",
            url: url,
            success: function (html) {
                wiktionnary.displayDef(html, lemma);
            }
        });
    },

    displayDef: function(def, lemma){
        if (this.modaleGenerated == false) {
            wiktionnary.createModale();
        }
        $('#wiktionnary-title').html(lemma);
        $('#wiktionnary-body').html(def);
        $('#wiktionnary').modal('show');
    },

    createModale: function(){
      var html = '<div id="wiktionnary" class="modal" tabindex="-1" role="dialog">';
      html += '<div class="modal-dialog" role="document">';
      html += '<div class="modal-content">';
      html += '<div class="modal-header">';
      html += '<h5 class="modal-title">'+ Translator.trans('definition') +' <strong id="wiktionnary-title"></strong></h5>';
      html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
      html += '<span aria-hidden="true">&times;</span>';
      html += '</button>';
      html += '</div>';
      html += '<div class="modal-body" id="wiktionnary-body">';
      html += '</div>';
      html += '<div class="modal-footer">';
      html += '<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>';
      html += '</div>';
      html += '</div>';
      html += '</div>';
      html += '</div>';

        wiktionnary.modaleGenerated == true;
        $("body").append(html);

        return;
    }
}
