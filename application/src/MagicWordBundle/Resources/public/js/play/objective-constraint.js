var objectiveConstraint = {
  properties: ["category", "number", "gender", "person", "mood", "tense"],
  constraintRealized: [],
  add: function(inflection) {
    var features = Object.values(gridJSON.inflections[inflection.toLowerCase()].features);
    var incrementedObjs = [];

    //on boucle sur les features
    //for (var i = 0; i < features.length; i++) {
    //var currentFeature = features[i];

    // on boucle sur les objectifs
    var constraints = Object.entries(roundJSON.constraints);
    for (var j = 0; j < constraints.length; j++) {
      var objective = constraints[j];
      var objectiveId = objective[1].id;

      // on évite les objectifs déjà complétés
      if (this.checkCompletion(objective) === false) {
        // on teste si les propriétés du mot match avec l'objectif
        if (this.isPertinent(objective, features)) {
          // Si l'objectif est pas dans incrementedObjs
          if (incrementedObjs.indexOf(objectiveId) === -1) {
            objectives.updateProgress(objectiveId);
            // on ajoute l'objectif aux constraintes réalisées
            this.constraintRealized.push(objectiveId);
            incrementedObjs.push(objectiveId);
            // on compte si le compte est bon...
            if (this.checkCompletion(objective)) {
              objectives.considerAsDone(objectiveId);
              activity.sendObjectiveDone(objectiveId);
            }
          }
        }

      }

    }
    //}
  },

  countByObjective: function(objectiveId) {
    var count = 0;
    for (var i = 0; i < this.constraintRealized.length; ++i) {
      if (this.constraintRealized[i] == objectiveId) {
        count++;
      }
    }

    return count;
  },

  isPertinent: function(objective, features) {
    var objectiveFeatures = Object.keys(objective[1].features);
    for (var i = 0; i < objectiveFeatures.length; i++) {
      var objectiveFeature = objectiveFeatures[i];
      if (features.indexOf(parseInt(objectiveFeature, 10)) === -1) {
        return false;
      }

    }
    return true;
  },

  checkCompletion: function(objective) {
    var objectiveId = objective[1].id;
    var numberToFind = objective[1].numberToFind;
    if (this.countByObjective(objectiveId) === numberToFind) {
      return true;
    }

    return false;
  }
}
