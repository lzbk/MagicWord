magicword
=========




# Notes
- Mariadb doit prendre en compte le fichier de conf dans docker-files
  - vérifier dans interface adminer, dans /variables, valeur token et stopword

- Pour import et suppression de lexiques -> le faire en commande car long

+# Command Adminer
+
+ALTER DATABASE dbmw CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
+ALTER TABLE lexicon_word CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
+ALTER TABLE lexicon_word_start CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
+ALTER TABLE lexicon_root CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
+ALTER TABLE lexicon_feature CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
+ALTER TABLE lexicon_letter CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
