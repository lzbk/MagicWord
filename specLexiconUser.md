Langue et description du lexique déposée par l'user
```
LANGUAGE=euskara
RELATIONTYPE=lemma
DESCRIPTION=Lexical data from Freeling
```
Nom de la relation (activant le combo) -> mot1 et mot2 ont en commun un lemme|racine|champ sémantique|...

Règles de réécriture (ordonnées)
```
RW:é,è,ê,ë,E,É=e
RW:à,â,ä,á,A,ã,Å,å=a
RW:û,ü,ù,U=u
RW:ô,ö,ó,O,õ=o
RW:ï,î,í,I=i
RW:C,ç=c
RW:B=b
RW:D=d
RW:F=f
RW:G=g
RW:H=h
RW:J=j
RW:K=k
RW:L=l
RW:M=m
RW:P=p
RW:Q=q
RW:R=r
RW:S=s
RW:T=t
RW:V=v
RW:W=w
RW:X=x
RW:Y=y
RW:Z=z
RW:-,',@,&, ,°,º,.,µ,\,/,:,`,+,~,;,^,"=
RW:ñ,N=n
RW:œ=oe
RW:æ=ae
```

```
Poids sur les lettres après réécriture
 - si lettre absente ici alors value =1
```
WEIGHT:b=2,c=3
```

Poids sur les longueurs de mots, avec x=y où `x` est égal à la longueur du mot et `y` au poids. Si pas de valeur spécifiée la longueur du mot est utilisée pour le poids.
```
LENGTH:2=1,3=2,4=3,5=4,6=5,7=6,8=7,9=8,10=9,11=10,12=11,13=12,14=13,15=14,16=15
```

Points sur les longueurs de combo, avec x=y où `x` est égal au nombre de mots du combo et `y` au points. Si pas de valeur spécifiée le combo vaut 1 point
```
COMBOPOINTS:2=5,3=10,4=15,5=20,6=25,7=30
```
